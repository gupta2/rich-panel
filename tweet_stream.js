const twit = require('twit');
var Twitter = require('twitter');
const config = require('../config/config');
const mysql = require('../config/mysqlQueries');
const sockets = require('../sockets-io');
function webhook()
{
	let stream_data = [];
	let stream_obj = {};
	stream_obj['consumer_key'] = config.twitter.consumer_key?'CQ9V3rPJPsAgF1JNfU3enJJ4S':config.twitter.consumer_key;
	stream_obj['consumer_secret'] = config.twitter.consumer_secret?'lIUGVBRafvU8tJDNNmWoqNgeH3Ak9NorS2TYziAGwfwK6L1SDA':config.twitter.consumer_secret;
	var client = new Twitter(stream_obj);
	let join_query = "select u.id,u.access_token_key,u.access_token_secret,t.user_screen_name,t.twitter_user_id from tweets t inner join users u on t.user_account_id = u.id";
	mysql.sqlQuery('rich',join_query,(data) => {
		for(let i in data)
		{
			stream_obj['access_token_key'] = data[i]['access_token_key'];
			stream_obj['access_token_secret'] = data[i]['access_token_secret'];
		}
		streams_data.push({stream:client.stream('statuses/filter', { track: [data[i]['user_screen_name']] }),userId:data[i]['twitter_user_id']})
	});
	listenForTweets(streams_data);		
}
function listenForTweets(streams_data){
    for(let stream of streams_data){
        stream.stream.on('tweet', (tweet)=>{
            console.log("tweeted")
            let email_id = req.query.email_id;
            let select_email_query = "select id from users where email_id = '"+email_id+"'";
            let select_query = "select * from tweets where tweet_id = '"+tweet.in_reply_to_status_id_str+"'";
            mysql.sqlQuery('rich',select_email_query,(data) => {
            	mysql.sqlQuery('rich',select_query,(result) => {		
					if(result.length == 0)
					{
						//Inserts the tweet into tweets table if no result found
						let insert_tweet_query = "insert into tweets (tweet_id,tweet_created_at,text,reply_status_id,twitter_user_id,twitter_user_name,user_screen_name,profile_image_url,user_account_id) values ";
						insert_tweet_query += "(" + JSON.stringify(tweet["id_str"]) + "," + JSON.stringify(tweet["created_at"]) + "," + JSON.stringify(tweet["text"]) + "," + JSON.stringify(tweet["in_reply_to_status_id_str"]) + "," + JSON.stringify(tweet["user"]["id_str"]) + "," + JSON.stringify(tweet["user"]["name"]) + "," + JSON.stringify(tweet["user"]["screen_name"]) + "," + JSON.stringify(tweet["user"]["profile_image_url_https"]) + "," + data[0]['id'] + ")"
						let socket = sockets.getSocket(tweet["id_str"]);
						socket.emit('tweet', tweet);
					}
					else
					{
						//Inserts the tweet into thread if there is result
						let insert_tweet_query = "insert into threads (parent_id,tweet_id,tweet_created_at,text,reply_status_id,twitter_user_id,twitter_user_name,user_screen_name,profile_image_url,user_account_id,tweet_id_constraint) values ";
						insert_tweet_query += "(" + JSON.stringify(result[0]["tweet_id"]) + "," + JSON.stringify(tweet["id_str"]) + "," + JSON.stringify(tweet["created_at"]) + "," + JSON.stringify(tweet["text"]) + "," + JSON.stringify(tweet["in_reply_to_status_id_str"]) + "," + JSON.stringify(tweet["user"]["id_str"]) + "," + JSON.stringify(tweet["user"]["name"]) + "," + JSON.stringify(tweet["user"]["screen_name"]) + "," + JSON.stringify(tweet["user"]["profile_image_url_https"]) + "," + data[0]['id'] + "," + result[0]['id'] + ")"	
						let socket = sockets.getSocket(tweet["id_str"]);
						socket.emit('thread', tweet);
					}
					saveTheTweet(insert_tweet_query);
				})
            });
        })
    }
}
function saveTheTweet(insert_tweet_query)
{
	mysql.sqlQuery('rich',insert_tweet_query,(result) => {
		console.log(result);
	})
}