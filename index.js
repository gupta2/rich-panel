const express = require('express');
const path = require("path");
const app = express();
const bodyParser = require("body-parser");
const config = require('./config/config.js');

const authCheckMiddleware = require('./config/authCheck.js');

const TwitterApi = require('node-twitter-signin')

const cors = require('cors');
const options = {
   allowedHeaders: ["Origin","Authorization","X-Requested-With","Content-Type", "Accept", "X-Access-Token"],
   credentials: true,
   methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
   origin: "*",
   preflightContinue: false
};
app.use(cors(options));
app.options("*", cors(options));


const auth = require('./routes/auth.routes');
const user = require('./routes/user.routes');

app.use('/rich-panel/',express.static(__dirname + '/dist'));

// app.get('/rich-panel/t/',TwitterApi(config.twitter.consumer_key, config.twitter.consumer_secret, 'http://35.244.0.27/rich-panel/user/twitter_callback', function(user,result){
// 		console.log(user,result)
// 	},function(res){
// 		return res.json({
//             'message':'connected',
//             "status":true
//           });
// 	})
// )


app.use(bodyParser.json({ limit: '800mb' }))
//app.use('/user', authCheckMiddleware);
app.use('/rich-panel/user', user);
app.use('/rich-panel/auth', auth);
app.get('/rich-panel/*', function(req, res) {
  res.sendFile(path.join(__dirname + '/dist/index.html'));
});
app.listen(1761);