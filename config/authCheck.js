const jwt = require('jsonwebtoken');
const config = require('./config');
const MySql = require("./mysqlQueries");
const mysql = require('mysql');
//when login is token based
module.exports = (req, res, next) => {
  if (!req.headers.authorization) return res.status(401).end();

  return jwt.verify(token, config.jwt_secret, (err, decoded) => {
      // the 401 code is for unauthorized status
      if (err) return res.status(401).end();
      //console.log(decoded);
      req.token = decoded;
      let inactive_query = "select * from users where email_id="+decoded.twitter_token+";"
      MySql.sqlQuery('saino_first',inactive_query,(err,result)=>{
        if(result.length>0)
        return next();
        else
          return res.status(401).end();
      });
      
    });
  
};