const config = require('../config/config.js');
const mysql = require('../config/mysqlQueries');
var TwitterApi = require("node-twitter-api")
var Twitter = require('twitter');
// const tw = new Twitter({
//   consumerKey: config.twitter.consumer_key,
//   consumerSecret: config.twitter.consumer_secret,
//   callback: 'http://35.244.0.27/rich-panel/user/twitter_callback'
// })
function getTweets(access_token_key,access_token_secret,callback)
{
	var client = new Twitter({
	  consumer_key: config.twitter.consumer_key?'CQ9V3rPJPsAgF1JNfU3enJJ4S':config.twitter.consumer_key,
	  consumer_secret: config.twitter.consumer_secret?'lIUGVBRafvU8tJDNNmWoqNgeH3Ak9NorS2TYziAGwfwK6L1SDA':config.twitter.consumer_secret,
	  access_token_key: access_token_key,
	  access_token_secret: access_token_secret
	});
	var params = {screen_name: 'nodejs'};
	client.get('statuses/mentions_timeline', params, function(error, tweets, response) {
	  console.log(tweets)
	  if (!error) {
	    callback(tweets)
	  }
	  else
	  {
	  	console.log(error)
	  	callback([])
	  }
	  	
	});
}

function getAccessTokes(email_id,callback)
{
	let query = "select id,access_token_key,access_token_secret from users where email_id='"+email_id+"'";
	mysql.sqlQuery("rich",query,(result)=>{
		if(result.length>0)
		{
			callback({status:true,id:result[0]["id"],access_token_key:result[0]["access_token_key"],access_token_secret:result[0]["access_token_secret"]})
		}
		else
		{
			callback({status:false})
		}
	});
}

exports.connectTwitter = (req,res)=>{
	req.email_id = "mail@mail.com"
	const tw = new TwitterApi({
	  consumerKey: config.twitter.consumer_key,
	  consumerSecret: config.twitter.consumer_secret,
	  callback: 'http://35.244.0.27/rich-panel/auth/twitter_callback?email_id='+req.email_id
	})

	tw.getRequestToken(function(err, requestToken, requestSecret) {
		console.log(err)
        if (err)
            res.status(500).send(err);
        else {
        	console.log(requestToken,requestSecret)
            _requestSecret = requestSecret;
            res.redirect("https://api.twitter.com/oauth/authenticate?oauth_token=" + requestToken);
        }
    });
}

exports.getMentionedTweets = (req,res) =>{
	let email_id = req.email_id?req.email_id:"mail@mail.com";
	console.log(email_id);
	let select_query = "select * from tweets where user_account_id = (select id from users where email_id ='"+email_id+"')";
	mysql.sqlQuery('rich',select_query,(data) => {
		return res.status(200).json({
			status : true,
			data : data
		})
	})
}
exports.getUniqueThread = (req,res) =>{
	let tweet_id = req.body.tweet_id;
	let select_query = "select * from threads where parent_id = '"+tweet_id+"'";
	mysql.sqlQuery('rich',select_query,(data) => {
		return res.status(200).json({
			status : true,
			data : data
		})
	})
}
exports.replyTweet = (req,res) =>{
	let query_params = req.query;
	let select_query = "select * from users where email_id = '"+query_params.email_id+"'";
	mysql.sqlQuery('rich',select_query,(result) => {
		var client = new Twitter({
		  consumer_key: config.twitter.consumer_key?'CQ9V3rPJPsAgF1JNfU3enJJ4S':config.twitter.consumer_key,
		  consumer_secret: config.twitter.consumer_secret?'lIUGVBRafvU8tJDNNmWoqNgeH3Ak9NorS2TYziAGwfwK6L1SDA':config.twitter.consumer_secret,
		  access_token_key: result[0]['access_token_key'],
		  access_token_secret: result[0]['access_token_secret']
		});
		let name_id = req.body.id_str;
		let reply = `${req.body.screen_name} ${req.body.text}`
		let params = {
			status : reply,
			in_reply_to_status_id : name_id
		}
		return client.post('statuses/update', params, function (err, data, response) {
            if (err !== undefined) {
                console.log(err);
                return res.status(500).json({
                	status:false,
                	message:"Failed to retweet"
                })
            } 
            else 
            {
                let new_tweet = {
                	created_at : data.created_at,
                	id_str : data.id_str,
                	text : data.text,
                	in_reply_to_status_id_str : data.in_reply_to_status_id_str,
                	user_id_str : data.user.id_str,
                	user_name : data.user.name,
                	user_screen_name : data.user.screen_name,
                	profile_image_url_https : data.user.profile_image_url_https 
                }
                new_tweet = new Twitter(new_tweet);
                new_tweet.parent_id = req.body.parent_id;
                new_tweet.user_account_id = req.user.userId
                new_tweet.save((err)=>{
                    console.log('Tweeted: ' + params.status);
                    return res.status(200).json({
                    	status:true,
                    	message:params.status
                    })  
                })
            }
        })
	})
}