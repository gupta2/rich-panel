const  Router  = require('express');
const  Auth = require('../controllers/auth.controller');
const router = new Router();


router.route('/login').post(Auth.login);
router.route('/register').post(Auth.register);
router.route('/twitter_callback').get(Auth.callbackTwitter);
module.exports = router;