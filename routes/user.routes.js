const  Router  =  require('express');
const  User = require('../controllers/user.controller');
const router = new Router();
const config = require('../config/config.js');
const TwitterApi = require('node-twitter-signin')
 
// const tw = LoginWithTwitter({
//   consumerKey: config.consumer_key,
//   consumerSecret: config.consumer_secret,
//   callbackUrl: '35.244.0.27/rich-panel/user/twitter/callback'
// })

router.route('/twitter_connect').get(User.connectTwitter);
	

router.route('/mentioned_tweets').get(User.getMentionedTweets);
router.route('/get_thread').post(User.getUniqueThread);
router.route('/store_tweet').post(User.replyTweet);
module.exports = router;